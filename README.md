# ONLINE_BANKING_SYSTEM
## Project Description
Banking Application is an API which serves the purpose of day to day user’s banking activities. The application will allow user to create user profile along with login credentials to the application. Once username and password created successfully, user will be granted access into the application. When user logs in, will be provided with the ability to  create checking or saving accounts, update profile, deposit cash, withdraw cash, view list of transactions and close account.
## Technologies Used
* Spring Boot
* Spring - Version 1.7.30
* Lombok - Version 1.18.16
* Java - Version 1.8
* Selenium Version - 3.141.59
* Jackson - Version -2.10.3
* Junit - Version 4.12
* Mockito - Version 1.10.19
* Mariadb - 2.6.2
* Javax Validation - 2.0.1 Final
* Amazon AWS - Version 4.12
* Cucumber - Version 6.7.0
* Java Mail - Version 1.5.0-b01
## Features
To-do list:
Update the passwords to be encrypted
Make sure that accounts cannot go negative
standardize table appearances
## Getting Started
open your favorite text editor. Run git clone https://gitlab.com/chirenjeebi/online-banking-java-springboot.git
## Usage
 Open up your favorite IDE. Run Project2Application.java.
Go to your favorite browser and go to http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com.
From there you can Register an user, input your email to retrieve your password if you forgot it. You can log into the system and register your accounts with the banking system. Then you can deposit and withdraw funds from accounts you have access to,
Transfer funds between two accounts you have access to, Update Profile information, View Statements for accounts, view your balance, and close accounts that have a balance of 0

## Contributors
John Lao, Alex Nelson, Abdulaziz Al biimani, Chirenjeebi Parajuli

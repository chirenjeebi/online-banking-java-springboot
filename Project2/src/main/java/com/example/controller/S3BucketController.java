package com.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.example.service.AmazonS3BucketService;
//@CrossOrigin(origins = "http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com/user/login")
@CrossOrigin(origins = "http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com")@Controller
public class S3BucketController {

    private AmazonS3BucketService amazonS3BucketService;
    
    S3BucketController(AmazonS3BucketService amazonS3BucketService) {
        this.amazonS3BucketService = amazonS3BucketService;
        
    }
    
    @CrossOrigin(origins = "http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com")
    @PostMapping("/uploadFile")
    public ResponseEntity<String> uploadFile(@RequestPart(value = "file") MultipartFile file) {
    	this.amazonS3BucketService.uploadFile(file);
    	return new ResponseEntity<>("photo uploaded", HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com")
    @PostMapping("/deleteFile")
    public String deleteFile(@RequestBody String fileURL) {
        return this.amazonS3BucketService.deleteFileFromBucket(fileURL);
    }
}
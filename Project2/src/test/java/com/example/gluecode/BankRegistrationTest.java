package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.BankLogin;
import com.example.e2e.page.BankRegistration;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BankRegistrationTest {
		
		public BankRegistration br;
		public BankLogin bl;
		
		@Given("a user is at the login page of bank application")
		public void a_user_is_at_the_login_page_of_bank_application() {
		    // Write code here that turns the phrase above into concrete actions
			this.bl = new BankLogin(BankDriverUtility.driver);
			this.br = new BankRegistration(BankDriverUtility.driver);
			assertEquals(BankLogin.title, BankDriverUtility.driver.getTitle());
		}

		@When("a user click register button")
		public void a_user_click_register_button() {
		    // Write code here that turns the phrase above into concrete actions
		    this.bl.registerLink.click();
		}
		@When("a user inputs first name {string}")
		public void a_user_inputs_first_name(String string) {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.firstName.sendKeys(string);
		}
		@When("then a user inputs last name {string}")
		public void then_a_user_inputs_last_name(String string) {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.lastName.sendKeys(string);
		}
		@When("then a user inputs contact Number  {string}")
		public void then_a_user_inputs_contact_number(String string) {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.contactNumber.sendKeys(string);
		}
		@When("then a user inputs address  {string}")
		public void then_a_user_inputs_address(String string) {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.address.sendKeys(string);
		}
		@When("then a user inputs age  {string}")
		public void then_a_user_inputs_age(String string) {
		    // Write code here that turns the phrase above into concrete actions
			this.br.age.sendKeys(string);
		}
		@When("then a user inputs username {string}")
		public void then_a_user_inputs_username(String string) {
		    // Write code here that turns the phrase above into concrete actions
			this.br.username.sendKeys(string);
		}
		@When("then a user inputs password  {string}")
		public void then_a_user_inputs_password(String string) {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.password.sendKeys(string);
		}
		@When("then a user inputs email {string}")
		public void then_a_user_inputs_email(String string) {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.email.sendKeys(string);
		}
		
		@When("then a user check the checkbox to agree to terms")
		public void then_a_user_check_the_checkbox_to_agree_to_terms() {
		    // Write code here that turns the phrase above into concrete actions

			this.br.agree.click();
		}
		
		@When("then submits the information")
		public void then_submits_the_information() {
		    // Write code here that turns the phrase above into concrete actions
		    this.br.registerButton.click();
		    System.out.println(this.br.registerButton.getText());
		}
		@Then("the user is redirected to the login page")
		public void the_user_is_redirected_to_the_login_page() {
		    // Write code here that turns the phrase above into concrete actions
		    WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
		    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/user/login"));
		    assertEquals("http://localhost:4200/user/login", BankDriverUtility.driver.getCurrentUrl());
		}



}

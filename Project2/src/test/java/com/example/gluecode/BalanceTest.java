package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BalanceTest {
	public BankLogin bl;
	public Bank b;
@Given("a user is logged in at the bank page wanting to go to Balance with {string} {string}")
public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_balance_with(String string, String string2) {
	bl = new BankLogin(BankDriverUtility.driver);
	b = new Bank(BankDriverUtility.driver);
	bl.username.sendKeys(string);
	bl.password.sendKeys(string2);
	bl.loginButton.click();
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));

}




@When("a user clicks the balance link")
public void a_user_clicks_the_balance_link() {
	b.balanceLink.click();

}
@Then("they are redirected to the balance page")
public void they_are_redirected_to_the_balance_page() {
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/balance"));
    assertEquals("http://localhost:4200/bank/balance", BankDriverUtility.driver.getCurrentUrl());
}

}
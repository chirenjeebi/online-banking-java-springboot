package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.Deposit;
import com.example.e2e.page.Withdrawal;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class WithdrawalTest {
	public BankLogin bl;
	public Bank b;
	public Withdrawal w;
@Given("a user is logged in at the bank page wanting to go to withdrawal with {string} {string}")
public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_withdrawal_with(String string, String string2) {
	bl = new BankLogin(BankDriverUtility.driver);
	b = new Bank(BankDriverUtility.driver);
	w = new Withdrawal(BankDriverUtility.driver);
	bl.username.sendKeys(string);
	bl.password.sendKeys(string2);
	bl.loginButton.click();
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));
}



@When("a user clicks the withdrawal link")
public void a_user_clicks_the_withdrawal_link() {
	b.withdrawLink.click();
}
@When("they are redirected to the withdrawal page")
public void they_are_redirected_to_the_withdrawal_page() {
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/withdraw"));
}
@When("they select an account number for withdrawal {string}")
public void they_select_an_account_number_for_withdrawal(String string) {
	w.getSelectOptions().selectByValue(string);
}
@When("they input a withdrawal amount {string}")
public void they_input_a_withdrawal_amount(String string) {
    w.withAmt.sendKeys(string);
}
@When("they input a withdrawal description {string}>")
public void they_input_a_withdrawal_description(String string) {
w.desc.sendKeys(string);
}
@Then("the user submits the withdrawal")
public void the_user_submits_the_withdrawal() {
w.subButton.click();
}



}

package com.example.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.controller.AccountController;
import com.example.controller.TransactionController;
import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.service.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;


@SpringBootTest
public class TransControllerTest{


	

	
	@InjectMocks
	private AccountController AController;
	
	
	@InjectMocks
	private TransactionController TController;
	
	@Mock
	private TransactionService tService;
	
	
	

	
	private MockMvc mock;
	
  
	LinkedHashMap<String, String> datapass = new LinkedHashMap<>();
	@BeforeEach
	public void setUp() throws Exception {
		ClientUser c = new ClientUser( "Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
				"starjohnson", "password", null, null);
		Branch b3 = new Branch("San Francisco");
		AccountType at2 = new AccountType("savings");
		Account a2 = new Account("132548102", 10000, c, at2, b3); 
		Account a3 = new Account("132548103", 0, c, at2, b3);
		double creditAmount =0;
		mock = MockMvcBuilders.standaloneSetup(TController).build();
		when(tService.deposit("132548102", 200, "paycheck")).thenReturn("Transaction succsessful");
		when(tService.withdraw("132548102", 200, "paycheck")).thenReturn("Transaction succsessful");
		when(tService.closeAccount("132548103")).thenReturn("Account successfully deleted");
		
		
	}
	
	

	
	@Test
	public void verifyPostdeposit() throws Exception {
		datapass.put("account_number", "132548102");
		datapass.put("creditAmount", "200");
		datapass.put("description", "paycheck");
		mock.perform(post("/api/transaction/deposit").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(datapass)))
		.andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Transaction succsessful"));
		
	}
	
	@Test
	public void verifyPostwithdraw() throws Exception {
		datapass.put("account_number", "132548102");
		datapass.put("debitAmount", "200");
		datapass.put("description", "paycheck");
		mock.perform(post("/api/transaction/withdraw").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(datapass)))
		.andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Transaction succsessful"));
		
	}
	
	@Test
	public void verifyPostdeletet() throws Exception {
		datapass.put("account_number", "132548103");
		datapass.put("email", "starjohnson@maildrop.cc");
		mock.perform(post("/api/transaction/delete").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(datapass)))
		.andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Account successfully deleted"));
		
	}
	
	
}




package com.example.eval.controller;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.controller.ClientUserController;
import com.example.model.ClientUser;
import com.example.service.ClientUserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
public class ClientUserControllerUnitTest {

	@Mock
	private ClientUserService cuServ;
	
	@InjectMocks
	private ClientUserController cuCon;
	
	private ClientUser cu1;
	private ClientUser cu2;
	private ClientUser cu3;
	private List<ClientUser> cList;

	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception{
		cu1 = new ClientUser(1,"johnlao","password","john","lao","santa paula", 23 ,"laojohnmatthew@gmail.com", "2246237364", null, null);
		cu2 = new ClientUser(2,"leooo","password","john","leo","santa paula", 24 ,"leojohnmatthew@gmail.com", "2246237364",  null, null);
		cu3 = new ClientUser(3,"starjohnson","password","Star", "Johnson", "Space", 55,"starjohnson@maildrop.cc","5015558888", null, null);
		cList = new ArrayList<>();
		cList.addAll(Arrays.asList(cu1,cu2,cu3));
		mock = MockMvcBuilders.standaloneSetup(cuCon).build();
		when(cuServ.getClientByUsername("johnlao")).thenReturn(cu1);
		when(cuServ.getClientByUsername("no such user")).thenReturn(null);
		when(cuServ.insertClient(cu1)).thenReturn(cu1);
		when(cuServ.getAllClient()).thenReturn(cList);;
		doNothing().when(cuServ).updateClient(cu1);
		when(cuServ.getClientByClientId(cu1.getClientId())).thenReturn(cu1);
	}
	
	@Test
	public void insertUserTestSuccess() throws Exception{
		mock.perform(post("/api/users/register").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(cu1)))
		.andExpect(status().isCreated()).andExpect(jsonPath("$").value("resource was created"));
	}
	
	@Test
	public void getClientUserByUsernameSuccess() throws Exception{
		
		mock.perform(get("/api/users/{username}", cu1.getUsername()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName", is(cu1.getFirstName())))
		.andExpect(jsonPath("$.lastName", is(cu1.getLastName())))
		.andExpect(jsonPath("$.address", is(cu1.getAddress())))
		.andExpect(jsonPath("$.contactNumber", is(cu1.getContactNumber())))
		.andExpect(jsonPath("$.username", is(cu1.getUsername())));
	} 
	
	@Test
	public void getClientUserByUsernameFailure() throws Exception{
		mock.perform(get("/api/users/{username}", "no such user"))
		.andExpect(status().isNotFound());
	} 
	
	@Test
	public void getAllClientSuccess() throws Exception{
		mock.perform(get("/api/users/{username}", cu1.getUsername()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName", is(cu1.getFirstName())))
		.andExpect(jsonPath("$.lastName", is(cu1.getLastName())))
		.andExpect(jsonPath("$.address", is(cu1.getAddress())))
		.andExpect(jsonPath("$.contactNumber", is(cu1.getContactNumber())))
		.andExpect(jsonPath("$.username", is(cu1.getUsername())));
	} 
	
	@Test
	public void updateClient() throws Exception {
		Map<String, String> updateMap = new LinkedHashMap<>();
		updateMap.put("firstName", cu1.getFirstName());
		updateMap.put("lastName", cu1.getLastName());
		updateMap.put("address", cu1.getAddress());
		updateMap.put("contactNumber", cu1.getContactNumber());
		updateMap.put("email", cu1.getEmail());
		updateMap.put("clientId", String.valueOf(cu1.getClientId()));
		mock.perform(put("/api/users/bank/profile/updateprofile").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(cu1)))
		.andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Profile Updated Successfylly"));
	}
}

package com.example.e2e.page;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
	features= {"feature/OnlineBankingRegister.feature","feature/OnlineBankingLogin.feature","feature/OnlineBankingLogout.feature","feature/OnlineBankingDeposit.feature","feature/OnlineBankingWithdraw.feature","feature/OnlineBankingFundTransfer.feature","feature/OnlineBankingBalanceView.feature","feature/OnlineBankingStatementView.feature","feature/OnlineBankingForgotPassword.feature","feature/OnlineBankingProfileCreateAccount.feature","feature/OnlineBankingProfileCloseAccount.feature","feature/OnlineBankingProfileHome.feature","feature/OnlineBankingProfileView.feature","feature/OnlineBankingProfileUpdate.feature"},
//		features= {"feature/OnlineBankingProfileCreateAccount.feature"},
		glue = {"com.example.gluecode"},
		monochrome = true,
		plugin = {"pretty"}
		)
public class TestRunner {
	
}

package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Profile {
	
	public Profile(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}

	@FindBy(linkText = "View Profile")
	public WebElement profileviewlink;
	
	@FindBy(linkText = "Update Profile")
	public WebElement profileupdatelink;

	@FindBy(linkText = "Create Account")
	public WebElement createAccountlink;
	
	@FindBy(linkText = "Close Account")
	public WebElement closeAccountlink;



}

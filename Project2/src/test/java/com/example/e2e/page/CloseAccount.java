package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CloseAccount {

	@FindBy(xpath = "//*[@id='accNum']")
	public WebElement accountnum;
	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-profile/app-close-account/div/div/button")
	public WebElement subButton;
	public CloseAccount(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public Select getSelectOptions1() {
		return new Select(accountnum);
	}}

package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Bank {

	@FindBy(linkText = "Balance")
	public WebElement balanceLink;
	@FindBy(linkText = "Home")
	public WebElement profile;
	@FindBy(linkText = "Logout")
	public WebElement logoutLink;
	@FindBy(linkText = "Deposit")
	public WebElement depositLink;
	@FindBy(linkText = "Withdraw")
	public WebElement withdrawLink;
	@FindBy(linkText = "Statement")
	public WebElement statementLink;
	@FindBy(linkText = "Fund Transfer")
	public WebElement fundTransferLink;
	

	public Bank(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}

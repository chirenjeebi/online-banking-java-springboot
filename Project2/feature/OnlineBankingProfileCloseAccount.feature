Feature: Bank Application Close Account
	As A user I wish to create accounts
	
	Scenario Outline: Close An Account
		Given a user is logged in at the bank page wanting to go to close account with "<username>" "<password>"
		When a user clicks onto th profile link
		When they are redirected to the profiles page
		When they click on the close account link
		When they input the accout number "<accountnum>"
		Then they submit the account close
	Examples:
		| username     | password  |accountnum|
		| johnlao      | password  |0200001002|
		| leooo        | password  |0100002001|
		
		

		
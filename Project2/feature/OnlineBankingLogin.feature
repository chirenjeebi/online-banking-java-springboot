Feature: Bank Application Login
	As A user I wish to log in to the banking application
	
	Scenario Outline: Login to Bank
		Given a user is at the login page of bank application ready to login
		When a a user inputs their username "<username>"
		And then a user inputs their password  "<password>"
		But then submits the login information
		Then the user is redirected to the bank page

	Examples:
		| username     | password  |
		| johnlao      | password  |
		| leooo        | password  | 
		| starjohnson  | password  |
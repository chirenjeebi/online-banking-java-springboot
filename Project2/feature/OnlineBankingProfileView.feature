Feature: Bank Application Profile View
	As A user I wish to log in to the banking application
	
	Scenario Outline: Get To Profile View
		Given a user is logged in at the bank page wanting to go to Profile View with "<username>" "<password>"
		When a user clicks on the profile link
		When a user is redirected to the profile home page
		And a user clicks the profile view link
		Then they are redirected to the profile view page


	Examples:
		| username     | password  |
		| johnlao      | password  |
		| leooo        | password  | 
		| starjohnson  | password  |
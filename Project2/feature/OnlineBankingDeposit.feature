Feature: Bank Application Deposit
	As A user I wish to make a deposit in my account
	
	Scenario Outline: Make a Deposit
		Given a user is logged in at the bank page wanting to go to Deposit with "<username>" "<password>"
		When a user clicks the deposit link
		And they are redirected to the deposit page
		And they select an account number for depositing "<accountNum>"
		And they input a deposit amount "<amount>"
		And they input a deposit description "<description">
		Then the user submits the deposit
	Examples:
		| username     | password  |  accountNum| amount |description| 
		| starjohnson  | password  |132548102   |  150   |paycheck|
		| johnlao      | password  |0300001001  | 100    |paydat|
		| leooo | password  |0300002001  | 100    |paydat|